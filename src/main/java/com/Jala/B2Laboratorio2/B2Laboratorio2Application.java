package com.Jala.B2Laboratorio2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class B2Laboratorio2Application {

    public static void main(String[] args) {
        SpringApplication.run(B2Laboratorio2Application.class, args);
    }
}
